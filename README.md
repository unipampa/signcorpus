# SignCorpus Annotator
A Web Tool for Building Parallel Corpora of Spoken and Sign Languages.

###Initial development:
* Portuguese <-> LIBRAS

###System Website:
  http://www.sistemas.porthal.com.br/SignCorpus

###System documentation:
  https://bitbucket.org/unipampa/signcorpus/wiki/Home

### Developer:
 - Alex Malmann Becker (Federal University of Pampa, Alegrete, Brazil)
 - Fábio Natanael Kepler (Federal University of Pampa, Alegrete, Brazil)

### Collaborator:
  - Sara Candeias (MSFT, Lisbon, Portugal)

### License:
  - Apache License